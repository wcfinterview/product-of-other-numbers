#Problem
You have a list of integers, and for each index you want to find the product of every integer except the integer at that index.

#Example
##Input:
[1, 7, 3, 4]

##Output:
[84, 12, 28, 21]

Because:
[7*3*4, 1*3*4, 1*7*4, 1*7*3]

#Do not use division