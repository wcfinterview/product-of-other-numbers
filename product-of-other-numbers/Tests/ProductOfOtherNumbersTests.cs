﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using product_of_other_numbers;
using System.Linq;

namespace Tests
{
    [TestClass]
    public class ProductOfOtherNumbersTests
    {
        [TestMethod]
        public void NullInputEmptyListOutput()
        {
            List<int> input = null;
            var result = ProductOfOtherNumbers.Calculate(input);
            Assert.IsTrue(!result.Any(), "Null input should return empty list.");
        }

        [TestMethod]
        public void EmptyListInputEmptyListOutput()
        {
            List<int> input = new List<int>();
            var result = ProductOfOtherNumbers.Calculate(input);
            Assert.IsTrue(!result.Any(), "empty input should return empty list.");
        }

        [TestMethod]
        public void OneElementInputOneElementOutput()
        {
            List<int> input = new List<int>() { 7842 };
            var result = ProductOfOtherNumbers.Calculate(input);
            Assert.IsTrue(result.Count == 1, "input of size 1 should have output of size 1");
            Assert.IsTrue(result[0] == 1, "output for input of size 1, should be 1");
        }

        [TestMethod]
        public void ExampleInputExampleOutput()
        {
            List<int> input = new List<int> {
                1,
                7,
                3,
                4,
            };

            List<int> expected = new List<int> {
                7 * 3 * 4,
                1 * 3 * 4,
                1 * 7 * 4,
                1 * 7 * 3,
            };

            var result = ProductOfOtherNumbers.Calculate(input);
            AssertContents(expected, result);
        }

        [TestMethod]
        public void InputWithZero()
        {
            List<int> input = new List<int> {
                1,
                7,
                3,
                0,
                4,
            };

            List<int> expected = new List<int> {
                7 * 3 * 0 * 4,
                1 * 3 * 0 * 4,
                1 * 7 * 0 * 4,
                1 * 7 * 3 * 4,
                1 * 7 * 3 * 0,
            };

            var result = ProductOfOtherNumbers.Calculate(input);
            AssertContents(expected, result);
        }

        [TestMethod]
        public void InputWithTwoZeros()
        {
            List<int> input = new List<int> {
                1,
                0,
                3,
                0,
                4,
            };

            List<int> expected = new List<int> {
                0 * 3 * 0 * 4,
                1 * 3 * 0 * 4,
                1 * 0 * 0 * 4,
                1 * 0 * 3 * 4,
                1 * 0 * 3 * 0,
            };

            var result = ProductOfOtherNumbers.Calculate(input);
            AssertContents(expected, result);
        }

        [TestMethod]
        public void InputWithNegatives()
        {
            List<int> input = new List<int> {
                1,
                -2,
                3,
                4,
            };

            List<int> expected = new List<int> {
                -2 * 3 * 4,
                1 * 3 * 4,
                1 * -2 * 4,
                1 * -2 * 3,
            };

            var result = ProductOfOtherNumbers.Calculate(input);
            AssertContents(expected, result);
        }

        [TestMethod]
        public void InputSet1()
        {
            List<int> input = new List<int> {
                1,
                2,
                3,
                4,
            };

            List<int> expected = new List<int> {
                2 * 3 * 4,
                1 * 3 * 4,
                1 * 2 * 4,
                1 * 2 * 3,
            };

            var result = ProductOfOtherNumbers.Calculate(input);
            AssertContents(expected, result);
        }


        [TestMethod]
        public void InputSet2()
        {
            List<int> input = new List<int> {
                14,
                23,
                32,
                41,
            };

            List<int> expected = new List<int> {
                23 * 32 * 41,
                14 * 32 * 41,
                14 * 23 * 41,
                14 * 23 * 32,
            };

            var result = ProductOfOtherNumbers.Calculate(input);
            AssertContents(expected, result);
        }

        [TestMethod]
        public void BigInputSet()
        {
            List<int> input = new List<int> {
                2,
                4,
                6,
                8,
                10,
                12,
                14,
                16,
            };

            List<int> expected = new List<int> {
                4 * 6 * 8 * 10 * 12 * 14 * 16,
                2 * 6 * 8 * 10 * 12 * 14 * 16,
                2 * 4 * 8 * 10 * 12 * 14 * 16,
                2 * 4 * 6 * 10 * 12 * 14 * 16,
                2 * 4 * 6 * 8 * 12 * 14 * 16,
                2 * 4 * 6 * 8 * 10 * 14 * 16,
                2 * 4 * 6 * 8 * 10 * 12 * 16,
                2 * 4 * 6 * 8 * 10 * 12 * 14,
            };

            var result = ProductOfOtherNumbers.Calculate(input);
            AssertContents(expected, result);
        }

        /// <summary>
        /// Helper method for asserting the contents of the 2 lists.
        /// </summary>
        /// <param name="expected">The expected list</param>
        /// <param name="result">The resulting list</param>
        private void AssertContents(List<int> expected, List<int> result)
        {
            Assert.AreEqual(expected.Count, result.Count, "Expected length of output to be the same as the input.");
            for (int i = 0; i < expected.Count; i++)
            {
                Assert.AreEqual(expected[i], result[i], string.Format("incorrect answer at index {0}", i));
            }
        }
    }
}
