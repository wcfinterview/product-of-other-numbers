﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace product_of_other_numbers
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Set your code up here
            List<int> input = new List<int> { 1, 7, 3, 4 };
            List<int> output = ProductOfOtherNumbers.Calculate(input);

            Console.WriteLine(string.Format("input: [{0}]", string.Join(",", input)));
            Console.WriteLine(string.Format("output: [{0}]", string.Join(",", output)));
            Console.ReadKey();
        }
    }
}
