﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace product_of_other_numbers
{
    /// <summary>
    /// You have a list of integers, and for each index you want to find the 
    /// product of every integer except the integer at that index.
    /// 
    /// Input:
    ///   [1, 7, 3, 4]
    /// 
    /// Output:
    ///   [84, 12, 28, 21]
    /// 
    ///   84 = 7 * 3 * 4,
    ///   12 = 1 * 3 * 4
    ///   28 = 1 * 7 * 4
    ///   21 = 1 * 7 * 3
    ///
    /// ** Do not use division
    /// 
    /// Gotchas
    /// Does your function work if the input list contains zeroes?
    /// We can do this in O(n) time and O(n) space!
    /// We only need to allocate one new list of size n.
    /// 
    /// Null input, or empty input returns empty list.
    /// Input of size 1, will return a list containing 1.
    /// </summary>
    public static class ProductOfOtherNumbers
    {
        public static List<int> Calculate(List<int> input)
        {
            List<int> output = new List<int>();

            // Your code here

            return output;
        }
    }
}
